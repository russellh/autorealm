FILE(GLOB source_files "*.cpp")

ADD_LIBRARY(load SHARED ${source_files} )
TARGET_LINK_LIBRARIES(load pluginengine renderengine ${wxWidgets_LIBRARIES} ${Pluma_LIBRARY} ${Boost_SERIALIZATION_LIBRARY})
