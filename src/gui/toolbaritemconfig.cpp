/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#include "toolbaritemconfig.h"
#include "appconfig.h"

ToolbarItemConfig::ToolbarItemConfig(fs::path const& rootDir)
:ItemConfig(rootDir)
{
	std::ifstream ifs(m_path.string());
	if(!ifs)
		throw std::runtime_error("unable to open configuration file: "+m_path.string());

	m_odFile.add_options()
		("label", po::value<std::string>(&m_label))
		("bitmap", po::value<std::string>(&m_bmp))
		;
	po::variables_map vm;
	store(parse_config_file(ifs, m_odFile), vm);
	notify(vm);

	m_bmp=AppConfig::buildPath(AppConfig::GRP_RES)+"/"+m_bmp;
	if(!fs::exists(fs::path(m_bmp)))
		throw std::runtime_error("file "+m_bmp+" does not exists but is refered by configuration file "+m_path.string()+"\n");
}

ToolbarItemConfig::ToolbarItemConfig(ToolbarItemConfig && other)
:ItemConfig(other)
,m_label(std::move(other.m_label))
,m_bmp(std::move(other.m_bmp))
{
}

ToolbarItemConfig::ToolbarItemConfig(ToolbarItemConfig const& other)
:ItemConfig(other)
,m_label(other.m_label)
,m_bmp(other.m_bmp)
{
}

std::string ToolbarItemConfig::label(void)const
{
	return m_label;
}

std::string ToolbarItemConfig::bmp(void)const
{
	return m_bmp;
}

wxAuiPaneInfo buildPaneInfo(Leaf<ToolbarItemConfig> &data)
{
	return wxAuiPaneInfo();
}
