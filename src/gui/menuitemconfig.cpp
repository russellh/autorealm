/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#include "menuitemconfig.h"
#include "id.h"

MenuItemConfig::MenuItemConfig(fs::path const& rootDir)
:ItemConfig(rootDir)
{
	m_odFile.add_options()
		("text", po::value<std::string>(&m_text))
		;
	po::variables_map vm;

	std::ifstream ifs(m_path.string());
	if(!ifs)
		throw std::runtime_error("unable to open file: "+m_path.string());
	store(po::parse_config_file<char>(ifs, m_odFile), vm);
	ifs.close();

	notify(vm);
}

MenuItemConfig::MenuItemConfig(MenuItemConfig && other)
:ItemConfig(other)
,m_text(std::move(other.m_text))
,m_showTitle(std::move(other.m_showTitle))
{
}

MenuItemConfig::MenuItemConfig(MenuItemConfig const& other)
:ItemConfig(other)
,m_text(other.m_text)
,m_showTitle(other.m_showTitle)
{
}

std::string MenuItemConfig::text(void)const
{
	return m_text;
}

bool MenuItemConfig::showTitle(void)const
{
	return m_showTitle;
}

wxMenuBar* createMenuFromFolder(Node<MenuItemConfig> & origin)
{
	wxMenuBar *menubar=new wxMenuBar();

	for(Node<MenuItemConfig>::iterator it=origin.begin();it!=origin.end();++it)
		if(it.isNode())
			menubar->Append(createMenu(dynamic_cast<Node<MenuItemConfig>&>(*it)),it->get().text());
		else
			throw std::runtime_error("menubar folder can only contain folders, because wxwidgets is only able to add wxMenu to wxMenubar");
	return menubar;
}

wxMenu* createMenu(Node<MenuItemConfig> & origin)
{
	///\note style is not supported because the only option is only available for GTK
	wxMenu *menu=origin.get().showTitle()?new wxMenu(origin.get().text()):new wxMenu();

	for(Node<MenuItemConfig>::iterator it=origin.begin();it!=origin.end();++it)
	{
		if(it.isNode())
		{
			Node<MenuItemConfig> &fold(dynamic_cast<Node<MenuItemConfig>&>(*it));
			menu->AppendSubMenu(createMenu(fold),fold.get().text(),fold.get().desc());
		}
		else
			menu->Append(it->get().id(),it->get().text(),it->get().desc(),it->get().kind());
	}

	return menu;
}
